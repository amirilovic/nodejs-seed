# NodeJS Seed

Seed project that provides configuration for:

- nvm (.nvm) - node version manager (https://github.com/nvm-sh/nvm)
- editorconfig (.editorconfig) - whitespace configuration for IDE (https://editorconfig.org/)
- prettier (.prettierrc, .prettierignore) - code formatting rules (https://prettier.io/)
- eslint (.eslintrc.js, .eslintignore) - code quality rules (https://eslint.org/)
- jest - test runner (https://jestjs.io/)

It also adds following npm tasks:

- start - runs `index.js`
- lint, lint:fix - runs eslint for all files in the project
- test - runs tests in the project using jest

For integration with VS Code please install:

- https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
- https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

To get use the project please **fork it**, clone new repo and then in new repo run:

```bash
$ npm install
```
